# E1.73 (Uniform Device Representation)

This repository contains supporting files for BSR E1.73, a draft ANSI standard for uniform program-readable descriptions of controllable devices designed for entertainment technology, building automation, and related industries.

The standard is in active development by the [ESTA](https://tsp.esta.org/tsp/index.html) [Control Protocols Working Group](https://tsp.esta.org/tsp/working_groups/CP/cp.html). Documentation of its current state of development can be found [here](https://esta-cpwg.gitlab.io/e173/udr-document.json). To collaborate on the draft standards and get more context on the contents of this repository, please [join](https://tsp.esta.org/tsp/working_groups/index.html) the CPWG.

## Development

Contribute to the schemas and libraries by opening merge requests against the `main` branch. Libraries are automatically validated against the corresponding document schema by CI.

Files contained within the `libraries` directory which do not form part of the BSR E1.73 schema must be prefixed with `_`.