"""Combine the document and archive schemas in this repo."""
import json
import os
import sys
import shutil

THIS_SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
REPO_ROOT = os.path.realpath(os.path.join(THIS_SCRIPT_DIR, ".."))
SCHEMAS_DIR = os.path.join(REPO_ROOT, "schemas")
SCHEMA_REF = 'esta:e173:'


def main():
    num_errors = 0

    print("Combinings schemas...")
    num_errors += combine_schemas()

    print()
    if num_errors == 0:
        print("All schemas were successfully combined.")

        print("Extracting newest schema... ")
        extract_newest_full_schema()
    else:
        print(f"Schemas failed to be combined with {num_errors} errors detected.")
    sys.exit(num_errors)

def extract_newest_full_schema():
    """ copies the newest full draft to the root directory.
    
    """

    # sort the directories by name asc
    dirs = os.listdir(SCHEMAS_DIR)
    dirs.sort()

    # get last entry
    entry = dirs[-1]

    full_schema_dir = os.path.join(SCHEMAS_DIR, entry, "full")

    if not os.path.exists(full_schema_dir):
        # issue error
        return

    file_origin = os.path.join(full_schema_dir, "udr-document.json")
    
    if not os.path.isfile(file_origin):
        # issue error
        return
    
    file_destination = os.path.join(REPO_ROOT, "udr-document.json")

    shutil.copyfile(file_origin, file_destination)

def combine_schemas() -> int:
    """Combines each UDR document schema in the 'schemas' directory with its sub-schemas.

    """
    num_errors = 0

    for entry in os.listdir(SCHEMAS_DIR):
        abs_schemas_dir = os.path.join(SCHEMAS_DIR, entry)

        if not os.path.isdir(abs_schemas_dir):
            continue

        schema_file = os.path.join(abs_schemas_dir, "udr-document.json")
        if not os.path.isfile(schema_file):
            print(
                f"Skipping {relative_to_repo_root(abs_schemas_dir)} because it does not have a document called udr-document.json."
            )
            continue
        
        schema = {}
        with open(schema_file, "r") as schema_file_handle:
            schema = json.load(schema_file_handle)
        
        for subentry in os.listdir(abs_schemas_dir):

            sub_schema_file = os.path.join(abs_schemas_dir, subentry)
            if not os.path.isfile(sub_schema_file) or subentry.startswith("_") or subentry == "udr-document.json" or subentry == "udr-archive.json":
                continue

            with open(sub_schema_file, "r") as sub_schema_file_handle:
                sub_schema = json.load(sub_schema_file_handle)
                key = sub_schema["$id"].removeprefix(SCHEMA_REF)

                del sub_schema["$id"]
                del sub_schema["$schema"]
                del sub_schema["title"]
                del sub_schema["description"]

                defs = None
                if "$defs" in sub_schema:
                    defs = sub_schema["$defs"]
                    for def_key in defs:
                        if def_key in schema["$defs"]:
                            if schema["$defs"][def_key] != defs[def_key]:
                                print(f"Duplicate sub-schema definition {def_key} discovered with different object information. Please resolve.")
                                num_errors += 1
                        else:
                            schema["$defs"][def_key] = defs[def_key]
                    del sub_schema["$defs"]

                # if no properties this is just a collection of defs
                if "properties" in sub_schema:
                    schema["$defs"][to_camel_case(key)] = sub_schema
        
        modify_string_for_key_in_json(schema, "$ref", SCHEMA_REF, "#/$defs/", True)

        full_schema_dir = os.path.join(abs_schemas_dir, "full")
        if not os.path.exists(full_schema_dir): 
            os.makedirs(full_schema_dir)

        if os.path.exists(full_schema_dir):
            full_schema = json.dumps(schema, indent=2)

            sub_schema_file = os.path.join(full_schema_dir, "udr-document.json")
            with open(sub_schema_file, "w") as schema_file:
                schema_file.write(full_schema)

    return num_errors

def modify_string_for_key_in_json(json_obj: dict, target_key: str, prefix: str, new_prefix: str, make_camel: bool):
    """Modifies the prefix of a string which is the value of a specified JSON key. 

    If make_camel is `true`, the value is also converted to camel case.
    """
    if isinstance(json_obj, dict):
        for key in list(json_obj.keys()):
            if key == target_key and json_obj[key].startswith(prefix):
                cut_index = json_obj[key].find(new_prefix) + len(new_prefix) if json_obj[key].find(new_prefix) > 0 else len(prefix)
                if make_camel:
                    json_obj[key] = new_prefix + to_camel_case(json_obj[key][cut_index:])
                else:
                    json_obj[key] = new_prefix + json_obj[key][cut_index:]
            else:
                json_obj[key] = modify_string_for_key_in_json(json_obj[key], target_key, prefix, new_prefix, make_camel)
    elif isinstance(json_obj, list):
        for i in range(len(json_obj)):
            json_obj[i] = modify_string_for_key_in_json(json_obj[i], target_key, prefix, new_prefix, make_camel)
    return json_obj

def to_camel_case(s: str):
    parts = s.split('-')
    return parts[0] + ''.join(word.capitalize() for word in parts[1:])

def relative_to_repo_root(file_name: str) -> str:
    return file_name.removeprefix(REPO_ROOT + os.sep)


if __name__ == "__main__":
    main()
